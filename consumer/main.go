package main

import (
	"fmt"
	"github.com/apache/pulsar-client-go/pulsar"
	_ "github.com/go-sql-driver/mysql"
	"github.com/golang/protobuf/proto"
	"github.com/jmoiron/sqlx"
	"log"
)

type Job struct {
	Title       string `json:"title"`
	Description string `json:"description"`
	Company     string `json:"company"`
	Salary      string `json:"salary"`
}

type App struct {
	db *sqlx.DB
}

func DBInit() *sqlx.DB {

	connectionString := "root:pass@tcp(192.168.225.113:3306)/test?multiStatements=true&sql_mode=TRADITIONAL&timeout=5s"
	var err error
	db, err := sqlx.Open("mysql", connectionString)

	if err != nil {
		log.Fatal(err)
	}
	return db
}

func (db Person) GetUserId() int32 {
	return db.Id
}

func main() {

	db := &App{db: DBInit()}

	client, err := pulsar.NewClient(pulsar.ClientOptions{URL: "pulsar://localhost:6650"})
	if err != nil {
		log.Fatal(err)
	}

	defer client.Close()

	channel := make(chan pulsar.ConsumerMessage, 100)

	options := pulsar.ConsumerOptions{
		Topic:            "topic-1",
		SubscriptionName: "my-subscription",
		Type:             pulsar.Shared,
	}

	options.MessageChannel = channel

	consumer, err := client.Subscribe(options)
	if err != nil {
		log.Fatal(err)
	}

	defer consumer.Close()

	// Receive messages from channel. The channel returns a struct which contains message and the consumer from where
	// the message was received. It's not necessary here since we have 1 single consumer, but the channel could be
	// shared across multiple consumers as well
	for cm := range channel {
		msg := cm.Message
		fmt.Printf("Received message  msgId: %v -- content: '%s'\n",
			msg.ID(), msg.Payload())

		newEmployee := &Person{}
		err = proto.Unmarshal(msg.Payload(), newEmployee)
		if err != nil {
			log.Fatal("unmarshaling error: ", err)
		}

		db.saveJobToDB(newEmployee)

		consumer.Ack(msg)

	}
}

func (dbp *App) saveJobToDB(newEmployee *Person) {

	tx := dbp.db.MustBegin()

	sql := `INSERT INTO job (title, description, company, salary) VALUES (?,?,?,?)`
	tx.MustExec(sql, newEmployee.Title, newEmployee.Description, newEmployee.Company, newEmployee.Salary)

	tx.Commit()

}
